## imports

source utils.sh

## set $INPUT_FOLDER if null

if [[ -z "$INPUT_FOLDER" ]]; then
  INPUT_FOLDER="./Inputs"
fi

for json_file in $INPUT_FOLDER/*.detect-faces.json; do
  image_file="$(basename "$json_file" .detect-faces.json)"
  rendered_image_file="$json_file.rendered.jpg"

  json=$(cat $json_file)

  for face in $(echo "${json}" | jq -c '.responses[].faceAnnotations[]'); do
    read x1 y1 <<< $(echo "${face}" | jq -r '.boundingPoly.vertices[0] | [.x, .y] | @tsv')
    read x2 y2 <<< $(echo "${face}" | jq -r '.boundingPoly.vertices[1] | [.x, .y] | @tsv')
    read x3 y3 <<< $(echo "${face}" | jq -r '.boundingPoly.vertices[2] | [.x, .y] | @tsv')
    read x4 y4 <<< $(echo "${face}" | jq -r '.boundingPoly.vertices[3] | [.x, .y] | @tsv')
    convert $INPUT_FOLDER/$image_file \
              -fill none -stroke red -strokewidth 5 \
              -draw "line $x1,$y1 $x2,$y2" \
              -draw "line $x2,$y2 $x3,$y3" \
              -draw "line $x3,$y3 $x4,$y4" \
              -draw "line $x4,$y4 $x1,$y1" \
              $rendered_image_file
  done
done
